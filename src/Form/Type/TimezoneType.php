<?php

namespace App\Form\Type;

use App\Entity\Timezone;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimezoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', Type\TimezoneType::class, array(
                'label' => 'label.timezone.name',
            ))
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'button.submit',
                'attr' => array(
                    'class' => 'btn btn-primary',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Timezone::class,
            'translation_domain' => 'app',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_timezone';
    }

    private function getTimezoneCondensed()
    {
        $sign = '';
        $i = -12;
        $timezones = [];
        while ($i <= 14) {
            $sign = ($i >= 0) ? '+' : '';
            $timezoneName = ($i == 0) ? 'GMT' : 'GMT'.$sign.$i.':00';
            $timezone = new \DateTimeZone($timezoneName);

            $date = new \Datetime('now', $timezone);

            $timezones['['.$timezoneName.'] - label.timezone.today : '.$date->format('d M H:i')] = $timezone->getName();
            $i++;
        }
        return $timezones;
    }
}
