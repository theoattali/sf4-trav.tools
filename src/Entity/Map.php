<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Map
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Server", inversedBy="maps")
     */
    private $server;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=300000000)
     */
    private $cities;

    /**
     * @ORM\Column(type="string", length=200000000)
     */
    private $players;

    /**
     * @ORM\Column(type="string", length=200000000)
     */
    private $allys;


    public function getId()
    {
        return $this->id;
    }

    public function setServer($server)
    {
        $this->server = $server;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function setUpdatedAt(\Datetime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setCities($cities)
    {
        $this->cities = $cities;
    }

    public function getCities()
    {
        return $this->cities;
    }

    public function setPlayers($players)
    {
        $this->players = $players;
    }

    public function getPlayers()
    {
        return $this->players;
    }

    public function setAllys($allys)
    {
        $this->allys = $allys;
    }

    public function getAllys()
    {
        return $this->allys;
    }
}
