<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180112001000 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE translation (id INT AUTO_INCREMENT NOT NULL, `key` VARCHAR(255) NOT NULL, message VARCHAR(255) NOT NULL, domain VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE map CHANGE cities cities LONGTEXT NOT NULL, CHANGE players players LONGTEXT NOT NULL, CHANGE allys allys LONGTEXT NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE translation');
        $this->addSql('ALTER TABLE map CHANGE cities cities LONGTEXT NOT NULL COLLATE utf8_unicode_ci, CHANGE players players LONGTEXT NOT NULL COLLATE utf8_unicode_ci, CHANGE allys allys LONGTEXT NOT NULL COLLATE utf8_unicode_ci');
    }
}
