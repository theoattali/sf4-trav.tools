(function (t) {
// fr
t.add("hello", "Hol\u00e0", "AppBundle", "fr");
t.add("map.city", "City", "AppBundle", "fr");
t.add("map.player", "Player", "AppBundle", "fr");
t.add("map.alliance", "Alliance", "AppBundle", "fr");
t.add("map.custom-points", "Custom points", "AppBundle", "fr");
t.add("map.see-in-travian", "see in travian", "AppBundle", "fr");
})(Translator);
