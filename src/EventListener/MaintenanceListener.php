<?php

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;


class MaintenanceListener
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($this->container->hasParameter('maintenance') && $this->container->getParameter('maintenance')) {

            $engine = $this->container->get('twig');
            $page = $engine->render('maintenance.html.twig');

            $event->setResponse(
              new Response($page, Response::HTTP_SERVICE_UNAVAILABLE)
            );
            $event->stopPropagation();
        }
    }
}
