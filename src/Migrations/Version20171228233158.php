<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171228233158 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE server (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, slug VARCHAR(255) NOT NULL, domain VARCHAR(255) NOT NULL, short_name VARCHAR(255) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, version VARCHAR(255) DEFAULT NULL, speed INT DEFAULT NULL, options LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', begin_at INT DEFAULT NULL, refresh_at DATETIME DEFAULT NULL, INDEX IDX_5A6DD5F6F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE map (id INT AUTO_INCREMENT NOT NULL, server_id INT DEFAULT NULL, updated_at DATETIME NOT NULL, cities LONGTEXT NOT NULL, players LONGTEXT NOT NULL, allys LONGTEXT NOT NULL, INDEX IDX_93ADAABB1844E6B7 (server_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE timezone (id INT AUTO_INCREMENT NOT NULL, timezone VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE server ADD CONSTRAINT FK_5A6DD5F6F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE map ADD CONSTRAINT FK_93ADAABB1844E6B7 FOREIGN KEY (server_id) REFERENCES server (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE map DROP FOREIGN KEY FK_93ADAABB1844E6B7');
        $this->addSql('ALTER TABLE server DROP FOREIGN KEY FK_5A6DD5F6F92F3E70');
        $this->addSql('DROP TABLE server');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE map');
        $this->addSql('DROP TABLE timezone');
    }
}
