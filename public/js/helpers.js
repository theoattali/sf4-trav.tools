function refreshCustomPoints() {
    d3.select('#customPoints').select('ul').remove();
    d3.select('#customPoints')
        .append('ul')
        .style('height', '115px')
        .style('overflow-y', 'scroll')
        .style('padding', '0 1.25rem .75rem')
        .style('margin-bottom', '0')
        .attr('class', 'list-unstyled')
        .selectAll('li')
        .data(customPoints)
        .enter()
        .append('li')
        .style('cursor', 'pointer')
        .attr('data-flag', function (d) { return d.flag; })
        .text(function (d) { return '(' + d.x + '|' + d.y + ')'; })
        .on('click', function(d) {
            zoom.translateTo(svg, d.x / d3.zoomIdentity.k, (d.y / d3.zoomIdentity.k) * -1);
            zoom.scaleTo(svg, scale.max);
        })
        .on('mouseenter', function() {
            d3.select(this).append('span')
                .attr('class', 'badge text-secondary')
                .html('<i class="fa fa-close"></i>')
                .on('click', function(d) {
                    d3.select('circle[x="'+ (d.x + circle.margin) +'"][y="'+ (d.y * -1 + circle.margin) +'"]').remove();
                    var index = customPoints.indexOf(d);
                    if (index > -1) {
                        customPoints.splice(index, 1);
                        refreshCustomPoints();
                    }
                });
        })
        .on('mouseleave', function() {
            d3.select(this).select('.badge').remove();
        }).filter('::before').style('color', 'red');

    travMap.selectAll('circle[data-flag]').remove();
    customPoints.forEach(function (point, i) {
        //var rectCustomPoints = d3.select('rect[x="'+ point.x +'"][y="'+ (point.y * -1) +'"]');
        travMap.append('circle')
            .attr('cx', point.x + circle.margin)
            .attr('cy', point.y * -1 + circle.margin)
            .attr('data-flag', point.flag)
            .attr('r', circle.r);
    });
}

function contextMenu() {
    d3.event.preventDefault(); // stop showing browser menu
    d3.select('#contextmenu').text('');

    d3.select('#contextmenu')
        .style('left', d3.event.pageX + 'px')
        .style('top', d3.event.pageY + 'px')
        .style('position', 'absolute')
        .style('display', 'block')
        .style('z-index', 1000);

    var loc = mouseCoords(d3.zoomIdentity.k, d3.mouse(d3.event.currentTarget)[0], d3.mouse(d3.event.currentTarget)[1]);

    d3.select('#contextmenu')
        .append('li')
        .attr('class', 'list-group-item list-group-item-action disabled')
        .html(
            '('+ loc.x +'|'+ loc.y +')'
            +'<a class="btn btn-link btn-sm" target="_blank" href="https://'+ gameworld_domain +'/karte.php?x='+ loc.x +'&y='+ loc.y +'">'
            + Translator.trans('map.see-in-travian')
            +'</a>'
        );

    d3.select('#contextmenu')
        .append('li')
        .style('padding-bottom', '0px')
        .attr('class', 'list-group-item list-group-item-action')
        .append('center')
        .selectAll('div')
        .data(flags)
        .enter()
            .append('div')
            .style('display', 'inline-block')
            .style('width', '15px')
            .style('height', '15px')
            .style('margin', '4px')
            .style('border', '1px solid rgba(0, 0, 0, .2)')
            .style('cursor', 'pointer')
            .style('background-color', function (d) { return d.value; })
            .on('click', function (d) {
                customPoints.push({ x: loc.x, y: loc.y, flag: d.color });

                refreshCustomPoints();
            });
}

function onGridContextMenu() {
    contextMenu();
}

function onRectContextMenu() {
    contextMenu();
    /*d3.event.preventDefault(); // stop showing browser menu
    d3.select('#contextmenu').text('');

    d3.select('#contextmenu')
        .style('left', d3.event.pageX + 'px')
        .style('top', d3.event.pageY + 'px')
        .style('position', 'absolute')
        .style('display', 'block')
        .style('z-index', 1000);

    var loc = mouseCoords(d3.zoomIdentity.k, d3.mouse(d3.event.currentTarget)[0], d3.mouse(d3.event.currentTarget)[1]);

    d3.select('#contextmenu')
        .append('li')
        .attr('class', 'list-group-item list-group-item-action disabled')
        .html(
            '('+ loc.x +'|'+ loc.y +')'
            +'<a class="btn btn-link btn-sm" href="https://{{ cities.gameworld.domain }}/karte.php?x='+ loc.x +'&y='+ loc.y +'">'
            +'{{ "see-in-travian"|trans }}'
            +'</a>'
        );*/
}

function mousemoved() {
    var loc = mouseCoords(d3.zoomIdentity.k, d3.mouse(d3.event.currentTarget)[0], d3.mouse(d3.event.currentTarget)[1]);
    info.text('('+ loc.x +'|'+ loc.y +')');
}

// Zoom
function zoomed() {
    travMap.attr('transform', d3.event.transform);
}

// Get current coordinates on mousemove
function mouseCoords(k, mx, my) {
    return {
        x: Math.floor(mx / k),
        y: Math.floor(my / k) * -1
    }
}

d3.selection.prototype.moveToFront = function() {
  return this.each(function() {
    this.parentNode.appendChild(this);
  });
}

/*Array.prototype.pushIfNotExist = function(element, data) {
    console.log("ok");
    if (this.indexOf(data) === -1) {
        this.push(element);
    }
}*/

function removeByValue(array, value) {
    return array.filter(function(elem, _index) {
        return value != elem;
    });
}

function rand(min, max) {
    return parseInt(Math.random() * (max-min+1), 10) + min;
}

function randomColor() {
    var h = rand(1, 360); // color hue between 1 and 360
    var s = rand(20, 100); // saturation 30-100%
    var l = rand(30, 70); // lightness 30-70%
    return 'hsl(' + h + ',' + s + '%,' + l + '%)';
}

function valBetween(v, min, max) {
    return Math.min(max, Math.max(min, v));
}
