<?php

namespace App\Entity;

class Timezone
{
    private $name = 'UTC';


    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}
