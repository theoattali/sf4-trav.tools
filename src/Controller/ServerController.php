<?php

namespace App\Controller;

use App\Entity\Country;
use App\Entity\Server;
use App\Entity\Timezone;
use App\Form\Type\TimezoneType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/server", name="server_")
 */
class ServerController extends Controller
{
    /**
     * @Route("/list", name="list")
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $countries = $em->getRepository(Country::class)->findAll();
        $servers = $em->getRepository(Server::class)->findAll();

        $timezone = new Timezone();
        $form = $this->createForm(TimezoneType::class, $timezone);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
        }

        return $this->render('server/list.html.twig', [
            'countries' => $countries,
            'servers' => $servers,
            'timezone' => $timezone,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{slug}", name="show", requirements={"slug"="[a-z0-9-]+"})
     */
    public function showAction(Server $server)
    {
        return $this->render('server/show.html.twig', [
            'server' => $server
        ]);
    }
}
