<?php

namespace App\Repository;

use App\Entity\Server;
use Doctrine\ORM\EntityRepository;

class ServerRepository extends EntityRepository
{
    public function getlastServer()
    {
        $qb = $this->createQueryBuilder('s');

        return $qb->getQuery()->setMaxResults(1)->getOneOrNullResult();
    }

    public function getNewServers()
    {
        $qb = $this->createQueryBuilder('s');
        $qb->where('s.beginAt > :beginAt')
           ->setParameter('beginAt', gmdate('U', strtotime('-10 day')));

        return $qb->getQuery()->getResult();
    }
}
