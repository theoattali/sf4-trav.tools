<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\Translation;

class TransExtractCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('trans:extract')
            ->setDescription('Extract translation')
            ->addArgument('locale', InputArgument::REQUIRED, 'Locale to extract');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $locale = $input->getArgument('locale');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $translator = $this->getContainer()->get('translator');
        $catalogue = $translator->getCatalogue($locale);
        $domains = $catalogue->all();
        while ($catalogue = $catalogue->getFallbackCatalogue()) {
            $domains = array_replace_recursive($catalogue->all(), $domains);
        }

        foreach ($domains as $domainKey => $domain) {
            foreach ($domain as $keyword => $message) {
                $translation = new Translation();
                $translation->setKeyword($keyword);
                $translation->setMessage($message);
                $translation->setDomain($domainKey);
                $translation->setLocale($locale);

                $em->persist($translation);
            }
        }
        $em->flush();
    }
}
