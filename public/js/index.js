var zoom = d3.zoom()
    .translateExtent([[maxCoords.left, maxCoords.bottom], [maxCoords.right, maxCoords.top]])
    .scaleExtent([scale.min, scale.max])
    .on('zoom', zoomed);

// -- Map
// Configure svg
var svg = d3.select('#travMap')
    .style('background-color', '#213A1C')
    .call(zoom);

var travMap = svg.append('g');

// Background grid
travMap.append('rect')
    .attr('id', 'grid')
    .attr('x', maxCoords.left)
    .attr('y', maxCoords.top * -1)
    .attr('width', width)
    .attr('height', height)
    .attr('fill', 'url(#mediumGrid)')
    .on('contextmenu', onGridContextMenu);

// Hide contextmenu on exterior click
d3.select(window).on('click', function() {
    if (d3.event.which == 1) {
        d3.select('#contextmenu')
            .style('display', 'none');
    }
});

// Populate map
cities.forEach(function (city, i) {
    var color = (randomColor)();
    travMap.append('rect')
        .attr('x', city.x)
        .attr('y', city.y * -1)
        .attr('width', rect.width)
        .attr('height', rect.height)
        .attr('data-type', 'player')
        .attr('data-ally', city.ally.id)
        .attr('data-player', city.player.id)
        .attr('data-ally-color', city.ally.color)
        .attr('data-player-color', city.player.color)
        .attr('fill', city.player.color)
        .style('stroke-width', 0.03)
        .style('stroke', 'white')
        .style('cursor', 'pointer')
        .on('mouseover', function () {
            travMap.on('mousemove', null);

            info.text('(' + city.x + '|' + city.y + ')');
            d3.select('.city-name').text(city.name);
            d3.select('.city-player').text(city.player.name);
            d3.select('.city-ally').text(!city.ally.name ? '-' : city.ally.name);
        })
        .on('mouseleave mouseout', function () {
            travMap.on('mousemove', mousemoved);

            d3.select('.city-name').text('-');
            d3.select('.city-player').text('-');
            d3.select('.city-ally').text('-');
        })
        //.on('contextmenu', onRectContextMenu);
});

var activeTab = null;

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var curr = e.target;
    if (curr.attributes.href.value == "#pane-players") {
    console.log("ok1");
        $('#travMap rect[data-player]').each(function() {
            var $t = $(this);
            $t.attr('fill', $t.attr('data-player-color'));
        });
    }
    else if (curr.attributes.href.value == "#pane-alliances") {
    console.log("ok2");
        $('#travMap rect[data-player]').each(function() {
            var $t = $(this);
            $t.attr('fill', $t.attr('data-ally-color'));
        });
    }
});

refreshCustomPoints();

d3.select('#pane-players')
    .style('padding-left', '0px')
    .selectAll('li')
    .data(players)
    .enter()
    .append('li')
    .append('span')
    .attr('class', 'name')
    .text(function (d) { return d.name; });
d3.select('#pane-alliances')
    .style('padding-left', '0px')
    .selectAll('li')
    .data(allys)
    .enter()
    .append('li')
    .attr('class', 'allys')
    .text(function (d) { return d.name; });

// Add repère
travMap.append('line').attr('x1', 0).attr('y1', (height / 2) * -1).attr('x2', 0).attr('y2', height / 2).style('stroke', '#E9C893').style('stroke-width', '0.1');
travMap.append('line').attr('x1', (width / 2) * -1).attr('y1', 0).attr('x2', width / 2).attr('y2', 0).style('stroke', '#E9C893').style('stroke-width', '0.1');
svg.append('a').attr('xlink:href', 'http://trav.tools').attr('target', '_blank').append('text').attr('id', 'curr-coords').attr('class', 'svg-infos').attr('x', svg.attr('width') - 150).attr('y', svg.attr('height') - 12).style('font-size', '12.5px').text('TravMap : http://trav.tools')

var info = svg.append('text')
    .attr('x', 12.5)
    .attr('y', 18)
    .attr('class', 'svg-infos')
    .style('font-size', '12px')
    .style('font-weight', 'bold');

travMap.on('mousemove', mousemoved);

// set default zoom values
zoom.translateTo(svg, 0, 0);
zoom.scaleTo(svg, 2.2);


