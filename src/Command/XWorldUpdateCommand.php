<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\XWorld;

class XWorldUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('trav:xworld:update')
            ->setDescription('Populate xworld')
            ->addArgument('domain', InputArgument::REQUIRED, 'Which domain name do you want pull?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argDomain = $input->getArgument('domain');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $domain = $em->getRepository('App:Gameworld')->findOneByDomain($argDomain);
        if ($argDomain != $domain->getDomain()) {
            $output->writeln('<error>Invalid domain argument</error>');
            return;
        }

        $xworld = $em->getRepository('App:XWorld')->findOneByGameworld(
            $domain
        );

        $output->writeln('Read online xworld list');
        $data = file_get_contents('http://'.$domain->getDomain().'/map.sql');

        $output->write('Check Last-Modified Date of file : ');
        $last_modified_date_header = $this->getLastModifiedDateHeader($http_response_header);
        if ($xworld !== null && $last_modified_date_header != $xworld->getUpdatedAt()) {
            $output->writeln('<error>Any new cities</error>');
            return;
        }
        $output->writeln('<info>OK</info>');

        $output->writeln('Translate SQL to JS (array of objects)');
        $pattern = '/INSERT INTO `x_world` VALUES \(\d+,(-?\d+),(-?\d+),\d+,(\d+),[\'"](.*)[\'"],(\d+),[\'"](.*)[\'"],(\d+),[\'"](.*?)[\'"],(\d+)\);/i';

        preg_match_all($pattern, $data, $out);

        $a_players = array();
        $s_players = '';
        $a_allys = array();
        $s_allys = '';
        $s_cities = '';
        foreach ($out[0] as $cityKey => $city) {
            $x = $out[1][$cityKey];
            $y = $out[2][$cityKey];
            $cid = $out[3][$cityKey];
            $name = $out[4][$cityKey];
            $pid = $out[5][$cityKey];
            $player = $out[6][$cityKey];
            $aid = $out[7][$cityKey];
            $ally = $out[8][$cityKey];
            $pop = $out[9][$cityKey];

            $pcolor = 'hsl('.rand(1, 360).','.rand(20, 100).'%,'.rand(30, 70).'%)';
            if (!isset($a_players[$player])) {
                $a_players[$player] = array(
                    'id' => $pid,
                    'name' => $player,
                    'color' => $pcolor,
                );
            }
            $pcolor = $a_players[$player]['color'];
            $player = '{id:'.$pid.',name:\''.$player.'\',color:\''.$pcolor.'\'}';

            $aid = ($pid == 1) ? -1 : $aid;
            $acolor = 'hsl('.rand(1, 360).','.rand(20, 100).'%,'.rand(30, 70).'%)';
            if (!isset($a_allys[$aid])) {
                $a_allys[$aid] = array(
                    'id' => $aid,
                    'name' => $ally,
                    'color' => ($aid == 0) ? '#EEE' : ($aid == -1) ? 'hsl(125,10%,15%)' : $acolor,
                );
            }
            $acolor = $a_allys[$aid]['color'];
            $ally = '{id:'.$aid.',name:\''.$ally.'\',color:\''.$acolor.'\'}';

            $s_cities .= '{x:'.$x.',y:'.$y.',cid:'.$cid.',name:\''.$name.'\',player:'.$player.',ally:'.$ally.',pop:'.$pop.',flag:\'\'},';
        }
        $s_cities = preg_replace('/,$/', '', $s_cities);
        $s_cities = '['.$s_cities.']';

        uksort($a_players, 'strcasecmp');
        uksort($a_allys, 'strcasecmp');

        foreach ($a_players as $key => $player) {
            $s_players .= '{id:'.$player['id'].',name:\''.$player['name'].'\',color:\''.$player['color'].'\'},';
        }

        foreach ($a_allys as $key => $ally) {
            $s_allys .= '{id:'.$ally['id'].',name:\''.$ally['name'].'\',color:\''.$ally['color'].'\'},';
        }

        $s_players = preg_replace('/,$/', '', $s_players);
        $s_players = '['.$s_players.']';
        $s_allys = preg_replace('/,$/', '', $s_allys);
        $s_allys = '['.$s_allys.']';

        $output->writeln('Persist new xworld');

        $xworld->setUpdatedAt($last_modified_date_header);
        $xworld->setCities($s_cities);
        $xworld->setPlayers($s_players);
        $xworld->setAllys($s_allys);
        $em->persist($xworld);

        $output->writeln('Flush');
        $em->flush();
    }

    private function getLastModifiedDateHeader($headers)
    {
        foreach ($headers as $k => $v) {
            $t = explode(':', $v, 2);
            if (isset($t[1]) && strtolower(trim($t[0])) == 'last-modified') {
                return new \Datetime(trim($t[1]));
            }
        }
        return false;
    }
}
