<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tool", name="tool_")
 */
class ToolController extends Controller
{
    /**
     * @Route("/event-calculator", name="event")
     */
    public function eventAction()
    {
        return $this->render('tool/event.html.twig');
    }
}
