<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServerRepository")
 */
class Server
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $domain;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shortName = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title = null;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="servers")
     * @ORM\JoinColumn(nullable=true)
     */
    private $country = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $version = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $speed = null;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $options = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $beginAt = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $refreshAt = null;

    /**
     * @ORM\OneToMany(targetEntity="Map", mappedBy="server", cascade={"ALL"})
     */
    private $maps;


    public function __construct()
    {
        $this->maps = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setDomain($domain)
    {
        $this->slug = preg_replace('/(.*)\.travian\.(.*)/', '$1.$2', $domain);
        $this->slug = preg_replace('/\./', '-', $this->slug);
        $this->domain = $domain;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    public function getShortName()
    {
        return $this->shortName;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setCountry(Country $country = null)
    {
        $this->country = $country;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    public function getSpeed()
    {
        return $this->speed;
    }

    public function setOptions($options)
    {
        $this->options = $options;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setBeginAt($beginAt)
    {
        $this->beginAt = $beginAt;
    }

    public function getBeginAt()
    {
        return $this->beginAt;
    }

    public function setRefreshAt(\Datetime $refreshAt)
    {
        $this->refreshAt = $refreshAt;
    }

    public function getRefreshAt()
    {
        return $this->refreshAt;
    }
}
