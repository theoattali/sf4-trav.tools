<?php

namespace App\DataFixtures;

use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CountryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'country_1' => array(
                'code' => 'ae',
                'name' => 'United Arab Emirates',
                'timezone' => 'Asia/Dubai',
            ),
            'country_2' => array(
                'code' => 'arabia',
                'name' => 'Arabia',
                'timezone' => 'Asia/Damascus',
            ),
            'country_3' => array(
                'code' => 'au',
                'name' => 'Australia',
                'timezone' => 'Australia/Canberra', // GMT +11:00
            ),
            'country_4' => array(
                'code' => 'ba',
                'name' => 'Bosnia and Herzegovina',
                'timezone' => 'Europe/Sarajevo',
            ),
            'country_5' => array(
                'code' => 'bg',
                'name' => 'Bulgaria',
                'timezone' => 'Europe/Sofia',
            ),
            'country_6' => array(
                'code' => 'br',
                'name' => 'Brazil',
                'timezone' => 'America/Sao_Paulo', // America/Sao_Paulo
            ),
            'country_7' => array(
                'code' => 'cl',
                'name' => 'Chile',
                'timezone' => 'America/Santiago',
            ),
            'country_8' => array(
                'code' => 'com',
                'name' => 'International',
                'timezone' => 'UTC',
            ),
            'country_9' => array(
                'code' => 'cz',
                'name' => 'Czech Republic',
                'timezone' => 'Europe/Prague',
            ),
            'country_10' => array(
                'code' => 'de',
                'name' => 'Germany',
                'timezone' => 'Europe/Berlin',
            ),
            'country_11' => array(
                'code' => 'dk',
                'name' => 'Denmark',
                'timezone' => 'Europe/Copenhagen',
            ),
            'country_12' => array(
                'code' => 'eg',
                'name' => 'Egypt',
                'timezone' => 'Africa/Cairo',
            ),
            'country_13' => array(
                'code' => 'es',
                'name' => 'Spain',
                'timezone' => 'Europe/Madrid',
            ),
            'country_14' => array(
                'code' => 'fi',
                'name' => 'Finland',
                'timezone' => 'Europe/Helsinki',
            ),
            'country_15' => array(
                'code' => 'fr',
                'name' => 'France',
                'timezone' => 'Europe/Paris',
            ),
            'country_16' => array(
                'code' => 'gr',
                'name' => 'Greece',
                'timezone' => 'Europe/Athens',
            ),
            'country_17' => array(
                'code' => 'hk',
                'name' => 'Hong Kong',
                'timezone' => 'Asia/Hong_Kong',
            ),
            'country_18' => array(
                'code' => 'hr',
                'name' => 'Croatia',
                'timezone' => 'Europe/Zagreb',
            ),
            'country_19' => array(
                'code' => 'hu',
                'name' => 'Hungary',
                'timezone' => 'Europe/Budapest',
            ),
            'country_20' => array(
                'code' => 'id',
                'name' => 'Indonesia',
                'timezone' => 'Asia/Jakarta',
            ),
            'country_21' => array(
                'code' => 'il',
                'name' => 'Israel',
                'timezone' => 'Asia/Jerusalem',
            ),
            'country_22' => array(
                'code' => 'ir',
                'name' => 'Iran',
                'timezone' => 'Asia/Tehran',
            ),
            'country_23' => array(
                'code' => 'it',
                'name' => 'Italy',
                'timezone' => 'Europe/Rome',
            ),
            'country_24' => array(
                'code' => 'jp',
                'name' => 'Japan',
                'timezone' => 'Asia/Tokyo',
            ),
            'country_25' => array(
                'code' => 'lt',
                'name' => 'Lithuania',
                'timezone' => 'Europe/Vilnius',
            ),
            'country_26' => array(
                'code' => 'my',
                'name' => 'Malaysia',
                'timezone' => 'Asia/Kuala_Lumpur',
            ),
            'country_27' => array(
                'code' => 'nl',
                'name' => 'Netherlands',
                'timezone' => 'Europe/Amsterdam',
            ),
            'country_28' => array(
                'code' => 'no',
                'name' => 'Norway',
                'timezone' => 'Europe/Oslo',
            ),
            'country_29' => array(
                'code' => 'pl',
                'name' => 'Poland',
                'timezone' => 'Europe/Warsaw',
            ),
            'country_30' => array(
                'code' => 'pt',
                'name' => 'Portugal',
                'timezone' => 'Europe/Lisbon',
            ),
            'country_31' => array(
                'code' => 'ro',
                'name' => 'Romania',
                'timezone' => 'Europe/Bucharest',
            ),
            'country_32' => array(
                'code' => 'rs',
                'name' => 'Serbia',
                'timezone' => 'Europe/Belgrade',
            ),
            'country_33' => array(
                'code' => 'ru',
                'name' => 'Russia',
                'timezone' => 'Europe/Moscow',
            ),
            'country_34' => array(
                'code' => 'sa',
                'name' => 'Saudi Arabia',
                'timezone' => 'Asia/Riyadh',
            ),
            'country_35' => array(
                'code' => 'se',
                'name' => 'Sweden',
                'timezone' => 'Europe/Stockholm',
            ),
            'country_36' => array(
                'code' => 'si',
                'name' => 'Slovenia',
                'timezone' => 'Europe/Ljubljana',
            ),
            'country_37' => array(
                'code' => 'sk',
                'name' => 'Slovakia',
                'timezone' => 'Europe/Bratislava',
            ),
            'country_38' => array(
                'code' => 'th',
                'name' => 'Thailand',
                'timezone' => 'Asia/Bangkok',
            ),
            'country_39' => array(
                'code' => 'tr',
                'name' => 'Turkey',
                'timezone' => 'Europe/Istanbul',
            ),
            'country_40' => array(
                'code' => 'tw',
                'name' => 'Taiwan',
                'timezone' => 'Asia/Taipei',
            ),
            'country_41' => array(
                'code' => 'uk',
                'name' => 'United Kingdom',
                'timezone' => 'Europe/London',
            ),
            'country_42' => array(
                'code' => 'us',
                'name' => 'United States',
                'timezone' => 'America/New_York',
            ),
            'country_43' => array(
                'code' => 'vn',
                'name' => 'Viet Nam',
                'timezone' => 'Asia/Ho_Chi_Minh',
            ),
        );

        foreach ($objects as $key => $object) {
            $country = new Country();
            $country->setCode($object['code']);
            $country->setName($object['name']);
            $country->setTimezone($object['timezone']);

            $manager->persist($country);

            $this->addReference($key, $country);
        }

        $manager->flush();
    }
}
