<?php

namespace App\Command;

use App\Entity\Country;
use App\Entity\Server;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ServerUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('trav:server:update')
            ->setDescription('Update server list');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $now = new \DateTime();

        $em = $this->getContainer()->get('doctrine')->getManager();

        $output->writeln('<info>Truncate server table</info>');
        $this->truncateEntity(Server::class);

        $output->writeln('<info>Read online server list</info>');
        $data = file_get_contents('http://status.travian.com');

        $output->writeln('<info>Get servers countries</info>');
        preg_match_all('/<td colspan="2"><div.*?id="([a-z]+)".*?>(.*?)<\/div><\/td>/s', $data, $gwCountries);

        $output->writeln('<info>Get servers domains by countries</info>');
        foreach ($gwCountries[2] as $gwCountryKey => $gwCountry) {
            preg_match_all('/<td.*?>(\w+\.travian.*?)<\/td>/', $gwCountry, $domains);
            foreach ($domains[1] as $gwDomainKey => $gwDomain) {
                $gw = new Server();
                $gw->setDomain($gwDomain);
                $country = $em->getRepository(Country::class)->findOneByCode($gwCountries[1][$gwCountryKey]);
                $gw->setCountry($country);
                $em->persist($gw);
            }
        }
        $em->flush();

        $output->writeln('<info>Get servers timestamps</info>');
        $data = file_get_contents('https://www.travian.com/international');
        $data = utf8_encode($data);

        preg_match('/"gameWorlds":{"loaded":".*,"forGtl":(.*?)},"cms"/', $data, $worlds);
        preg_match_all('/<label class="radioButton" for="lang-([a-z]+)-others">.*<span class=" language">(.*?)<\/svg><\/span>/', $data, $flags);

        $worlds = json_decode($worlds[1]);
        foreach ($worlds as $key => $world) {
            preg_match('/^https?:\/\/([a-z0-9\.\-]+)\/?$/', $world->url, $out);
            $findGw = $em->getRepository(Server::class)->findOneByDomain($out[1]);
            if (null !== $findGw) {
                $findGw->setBeginAt($world->start);
                $findGw->setTitle($world->name);
                $findGw->setShortName($world->shortcut);
                $findGw->setRefreshAt($now);
                $em->persist($findGw);
            }
            else {
                $gw = new Server();
                $gw->setDomain($out[1]);
                $gw->setRefreshAt($now);
                $gw->setBeginAt($world->start);
                $gw->setTitle($world->name);
                $gw->setShortName($world->shortcut);
                $gw->setRefreshAt($now);
                $em->persist($gw);
            }
        }

        $em->flush();

        $output->writeln('<info>Get new Servers timestamps</info>');
        $gws = $em->getRepository(Server::class)->findAll();
        foreach ($gws as $key => $gw) {

            $data = @file_get_contents('http://'.$gw->getDomain());
            if ($data === false) {
                $em->remove($gw);
                continue;
            }

            preg_match('/Travian\.Game\.version = \'(.*)\';/', $data, $version);
            if (!isset($version[1])) {
                $em->remove($gw);
                continue;
            }
            $gw->setVersion($version[1]);
            preg_match('/Travian\.Game\.speed = (\d+);/', $data, $speed);
            $gw->setSpeed($speed[1]);
            preg_match('/var T4_feature_flags = (.*);[\W]+<\/script>/', $data, $options);
            $gw->setOptions(json_decode(utf8_encode($options[1])));

            if ($gw->getCountry() == null) {
                preg_match('/Travian\.Game\.country = \'(.*)\';/', $data, $country);
                $_country = array(
                    'en' => 'com',
                    'sy' => 'arabia',
                );
                if (isset($_country[$country[1]])) {
                    $country = $em->getRepository(Country::class)->findOneByCode($_country[$country[1]]);
                }
                else {
                    $country = $em->getRepository(Country::class)->findOneByCode($country[1]);
                }

                $gw->setCountry($country);
            }

            if ($gw->getTitle() == null) {
                preg_match('/http.*\?server=(.*)#register/', $data, $shortName);
                $gw->setShortName($shortName[1]);
                $gw->setTitle($shortName[1]);
            }

            if ($gw->getBeginAt() == null) {
                preg_match('/<span class="date">(\d{1,2})\.(\d{1,2})\.(\d{1,2}) (\d{2}):(\d{2})<span class="timezone">.* ([\+\-]\d{2})/', $data, $out);
                if (isset($out[3])) {
                    $timestamp = strtotime($out[3]."-".$out[2]."-".$out[1]." ".($out[6] - $out[4]).":".$out[5]);
                    $gw->setBeginAt($timestamp);
                }
                if ($gw->getBeginAt() == null) {
                    $em->remove($gw);
                    continue;
                }
            }

            $em->persist($gw);
        }

        $output->writeln('<info>Flush new servers</info>');
        $em->flush();
    }

    private function truncateEntity($name) {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $cmd = $em->getClassMetadata($name);
        $connection = $em->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
        $connection->executeUpdate($q);
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
        $em->flush();
    }
}
