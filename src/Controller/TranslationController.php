<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;


use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Dumper\PhpFileDumper;

use App\Entity\Translation;

/**
 * @Route("/tr", name="tr_")
 */
class TranslationController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $translations = $em->getRepository(Translation::class)->findAll();
        $locale = $em->getRepository(Translation::class)->getExtractedLocale();
        $domain = $em->getRepository(Translation::class)->getExtractedDomain();

        return $this->render('translation/index.html.twig', [
            'translations' => $translations,
            'locale' => $locale,
            'domain' => $domain,
        ]);
    }
}
