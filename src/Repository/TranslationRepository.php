<?php

namespace App\Repository;

use App\Entity\Translation;
use Doctrine\ORM\EntityRepository;

class TranslationRepository extends EntityRepository
{
    public function getExtractedLocale()
    {
        $qb = $this->createQueryBuilder()
            ->select('t')
            ->groupBy('t.locale');

        return $qb->getQuery()->getResult();
    }

    public function getExtractedDomain()
    {
        $qb = $this->createQueryBuilder('t')
            ->select('t')
            ->groupBy('t.domain');

        return $qb->getQuery()->getResult();
    }
}
